#include <math.h>
#include <algorithm>
#include <stdlib.h>
#include <time.h>


/*
 * Game takes place on a rectangular area on the plane. The game logic and all
 * objects locations/dimensions are described by real numbers:
 * <0..1>X<0..ASPECT_RATIO>
 *
 * ASPECT_RATIO is calculated from window size: hight/width. When objects are
 * reentered all the dimensions are translated to real numbers from:
 * <0..WIDTH>X<0..HEIGHT> by multiplying through the window width.
 * */

#define AR ((double)app.h/app.w)


class geometry
{
public:
   struct P
   {
      P(): x(0), y(0) {}
      P(const P& o): x(o.x), y(o.y) {}
      P(double x_, double y_): x(x_), y(y_) {}
      double x;
      double y;
   };

   typedef P Vect;

   struct Circle: public P
   {
      Circle(): P(), r(0) {}
      Circle(double x_, double y_, double r_): P(x_, y_), r(r_) {}
      double r;
   };

   struct Arc: public Circle
   {
      Arc(): Circle(), a1(0), a2(0) {}
      Arc(double x, double y, double r, double a1_, double a2_):
         Circle(x, y, r), a1(a1_), a2(a2_) {}
      double a1;
      double a2;
   };

   /* Find intersection betwen two parametric lines */
   static void intersec(double a1, double b1, double a2, double b2, P& p)
   {
      p.x = (b2-b1)/(a1-a2);
      p.y = p.x*a1+b1;
   }

   struct Line
   {
      P p1, p2;
      Line(): p1(), p2() {}
      Line(const P& a, const P& b): p1(a), p2(b) {}
      Line(double p1x, double p1y, double p2x, double p2y)
         : p1(p1x, p1y), p2(p2x, p2y) {}
      void parametric(double& a, double& b) const
      {
         a = (p1.y - p2.y)/(p1.x - p2.x);
         b =  p1.y - p1.x*a;
      }
      void perpendicular(const P& p, double& a, double& b) const
      {
         double la = (p1.y - p2.y)/(p1.x - p2.x);
         a = -1/la;
         b = p.y - a*p.x;
      }
      void closest_point_on_line(const P& p, P& c) const
      {
         if (p1.y == p2.y)
         {
            c.y = p1.y;
            c.x = p.x;
            return;
         }
         if (p1.x == p2.x)
         {
            c.x = p1.x;
            c.y = p.y;
            return;
         }
         double perpend_a, perpend_b;
         double a, b;
         perpendicular(p, perpend_a, perpend_b);
         parametric(a, b);
         intersec(a, b, perpend_a, perpend_b, c);
      }
      double ang() const
      {
         double x = p2.x - p1.x;
         double y = p2.y - p1.y;
         return atan2(x, y);
      }
   };

   static inline double mod(double x, double y)
   {
      return sqrt(x*x+y*y);
   }

   static inline double mod(P& p)
   {
      return sqrt(p.x*p.x+p.y*p.y);
   }

   /*
    * Time to colision between Circle and the edge.
    * */
   static double time_to_colision(const Circle& s, const Vect& v, double xl, double yl)
   {
      double xdistance = v.x > 0 ?
         xl - s.x - s.r:
         -1*(s.x  - s.r);
      double tx = v.x? xdistance/v.x
         : 10000000;

      double ydistance = v.y > 0 ?
         yl - s.y - s.r:
         -1*(s.y  - s.r);
      double ty = v.y? ydistance/v.y
         : 10000000;

      return std::min(tx, ty);
   }

   static int solve_quadratic_eq(double a, double b, double c, double& x1, double& x2)
   {
      double delta = b*b-4*a*c; 
      if (delta < 0)
         return 0; // No solution.

      x1 = (-b-sqrt(delta))/(2*a);
      x2 = (-b+sqrt(delta))/(2*a);
      return 2;
   }

   /*
    * Time to colision between two Circles objects.
    * */
   static double time_to_colision(const Circle& s1, const Circle& s2,
         const Vect& v1, const Vect& v2)
   {
      // the equation is (s1.r+s2.r)^2 = (distance on x )^2 + (distance on y)^2,
      // which gives:
      //
      // (s1.r+s2.r)^2
      //    = (s1.x +v1.x*t -s2.x -v2.x*t)^2
      //    + (s1.y +v1.y*t -s2.y -v2.y*t)^2
      //
      // (s1.r+s2.r)^2
      //    = (s1.x-s2.x + t*(v1.x-v2.x))^2
      //    + (s1.y-s2.y + t*(v1.y-v2.y))^2
      //
      // (s1.r+s2.r)^2
      //    = (s1.x-s2.x)^2 + 2t*(s1.x-s2.x)*(v1.x-v2.x) +(v1.x-v2.x)^2*t^2
      //    + (s1.y-s2.y)^2 + 2t*(s1.y-s2.y)*(v1.y-v2.y) +(v1.y-v2.y)^2*t^2;
      //
      // written as equation on t:
      // t^2((v1.y-v2.y)*(v1.y-v2.y)+(v1.x-v2.x)*(v1.x-v2.x))
      //   + 2t((s1.x-s2.x)*(v1.x-v2.x)+(s1.y-s2.y)*(v1.y-v2.y))
      //   + (s1.x-s2.x)^2+(s1.y-s2.y)^2 - (s1.r+s2.r)^2
      //   = 0
      //
      // lets calculate it as a*t^2 + b*t +c = 0
      double a = (v1.y-v2.y)*(v1.y-v2.y)    + (v1.x-v2.x)*(v1.x-v2.x);
      double b = 2*((s1.x-s2.x)*(v1.x-v2.x) + (s1.y-s2.y)*(v1.y-v2.y));
      double c = (s1.x-s2.x)*(s1.x-s2.x)    + (s1.y-s2.y)*(s1.y-s2.y)
         - (s1.r+s2.r)*(s1.r+s2.r);

      // Now just to solve the equation. 
      double x1, x2;
      int no = solve_quadratic_eq(a,b,c, x1, x2);
      if (no == 0)
         return 10000000; // No solution.

      // Return the lower positive solution.
      if (x1 >= 0)
         return x1;

      if (x2 >= 0)
         return x2;

      return 10000000; // No solution.
   }

   static double time_to_colision(const Circle& s1, const Vect& v, const Arc& s2)
   {
      // Same as above, only v2 is 0.
      double a = (v.y)*(v.y)           + (v.x)*(v.x);
      double b = 2*((s1.x-s2.x)*(v.x)   + (s1.y-s2.y)*(v.y));
      double c = (s1.x-s2.x)*(s1.x-s2.x) + (s1.y-s2.y)*(s1.y-s2.y)
         - (s1.r+s2.r)*(s1.r+s2.r);
      double x1, x2;
      int no = solve_quadratic_eq(a,b,c, x1, x2);
      if (no == 0)
         return 10000000; // No solution.

      /* check if the solution is withing the part of circle that the arc is using*/
      if (x1 >= 0)
      {
         P p(s1.x + v.x*x1, s1.y+v.y*x1);
         Line l(p, s2);
         double x = l.ang();
         if (s2.a1 <= x && x < s2.a2)
         {
            return x1;
         }
      }
      if (x2 >= 0)
      {
         P p(s1.x + v.x*x2, s1.y+v.y*x2);
         Line l(p, s2);
         double x = l.ang();
         if (s2.a1 <= x && x < s2.a2)
         {
            return x2;
         }
      }
      return 10000000;
   }

   static double time_to_colision(const Circle& s, const Vect& v, const Line& l)
   {
      double t, t1, t2;
      if (l.p1.x == l.p2.x)
      {
         if (!v.x)
            return 10000000;

         double t = std::min(l.p1.x + s.r - s.x, l.p1.x - s.r - s.x)/v.x;
         double y = s.y+v.y*t;
         if (std::min(l.p1.y,l.p2.y) <= y && y <= std::max(l.p1.y,l.p2.y))
            return t>0? t:10000000;

         return 10000000;
      }

      double a, b, c1, c2;
      P d;
      l.parametric(a, b);
      c1 = b + s.r/sin(l.ang());
      c2 = b - s.r/sin(l.ang());
      if (!a*v.x-v.y)
         return 10000000;

      t = std::min(s.y-a*s.x-c1, s.y-a*s.x-c1)/(a*v.x-v.y);
      if (t<0)
         return 10000000;

      double y = s.y+v.y*t;
      double x = s.y+v.y*t;
      l.closest_point_on_line(P(x,y), d);

      if (std::min(l.p1.x,l.p2.x) <= d.x && d.x <= std::max(l.p1.x, l.p2.x)
            && std::min(l.p1.y,l.p2.y) <= d.y && d.y <= std::max(l.p1.y, l.p2.y))
         return t;
      return 10000000;
   }

   class RoundedRect
   {
   public:
      P p1, p2;
      double r;

   };
   static double time_to_colision(const Circle& s, const Vect& v, const RoundedRect& rr)
   {
      double r = rr.r;
      P p1 = rr.p1;
      P p2 = rr.p2;

      double t = 10000000;
      t = std::min(t, time_to_colision(s, v, Line(P(p1.x+r, p1.y), P(p2.x-r, p1.y))));
      t = std::min(t, time_to_colision(s, v, Line(P(p2.x, p1.y+r), P(p2.x, p1.y-r))));
      t = std::min(t, time_to_colision(s, v, Line(P(p1.x+r, p2.y), P(p2.x-r, p2.y))));
      t = std::min(t, time_to_colision(s, v, Line(P(p1.x, p1.y+r), P(p1.x, p2.y-r))));

      t = std::min(t, time_to_colision(s, v, Arc(p1.x+r, p1.y+r, r, 1*agg::pi, 1.5*agg::pi)));
      t = std::min(t, time_to_colision(s, v, Arc(p2.x+r, p1.y+r, r, 1.5*agg::pi, 0)));
      t = std::min(t, time_to_colision(s, v, Arc(p2.x-r, p2.y-r, r, 0, 0.5*agg::pi)));
      t = std::min(t, time_to_colision(s, v, Arc(p1.x+r, p2.y-r, r, 0.5*agg::pi, 1*agg::pi)));
      return t;
   }
};

