class Target
{
public:
   Target(App& application,
         double s, double pos_x, double pos_y,
         agg::rgba c):
      app(application), x(pos_x), y(pos_y), size(s), color(c), i(0) { }
   double x,y;
   double size;
   agg::rgba color;
   App& app;
   int i;

   void draw(agg::rasterizer_scanline_aa<>& ras,
         agg::scanline_u8& sl,
         renderer_base_type& rbase)
   {
      renderer_scanline_type ren_sl(rbase);
      ren_sl.color(color);
      agg::ellipse e1;
      agg::ellipse e2;
      ras.reset();
      double f   = (double)(i++%20)/20;
      double f_1 = 1-f;
      e1.init(app.w*x, app.w*y, app.w*size*f, app.w*size*f_1, 20);
      e2.init(app.w*x, app.w*y, app.w*size*f_1, app.w*size*f, 20);
      ras.add_path(e1);
      ras.add_path(e2);
      agg::render_scanlines(ras, sl, ren_sl);
   }
   void newLocation()
   {
      do {
         x = (double)rand()/RAND_MAX;
         y = (double)rand()/RAND_MAX*AR;
         // Don't put the target in any of the coroners or very close to the
         // edge.
      } while (false
            || x < 0.01 || x > 0.99
            || y < 0.01 || y > 0.99
            || (x < 0.03 && y < 0.03)
            || (x < 0.03 && y > 0.97*AR)
            || (x > 0.97 && y < 0.04)
            || (x > 0.97 && y > 0.97*AR)
            );
   }
};

class Ball;
class Target;
class Control
{
public:
   virtual void getAcc(double& acc_x, double& acc_y, Ball&, Target& t) = 0;
   virtual void draw(agg::rasterizer_scanline_aa<>& ras,
         agg::scanline_u8& sl,
         renderer_base_type& rbase) {};
   int level;
};


class Ball
{
public:
   Ball(App& application,
         double s, double m, double pos_x, double pos_y,
         agg::rgba c, int img, Control* ctrl):
      app(application),
      mass(m), c(pos_x, pos_y, s), v(0, 0),
      controller(ctrl), color(c), img_idx(img),
      force_field(0.3) { }
   double mass;
   geometry::Circle c;   // location & size
   geometry::Vect   v;   // velocity
   double force_field;
   int    img_idx;
   agg::rgba color;
   App& app;
   Control* controller;

   void updateV(Target& target, double t)
   {
      double acc_x, acc_y;
      controller->getAcc(acc_x, acc_y, *this, target);
      v.x += t*acc_x;
      v.y += t*acc_y;
   }

   void draw(agg::rasterizer_scanline_aa<>& ras,
         agg::scanline_u8& sl,
         renderer_base_type& rbase)
   {
      renderer_scanline_type ren_sl(rbase);
      color.opacity(force_field);
      ren_sl.color(color);
      ras.reset();
      agg::ellipse e;
      e.init(app.w*c.x, app.w*c.y, app.w*c.r, app.w*c.r, 30);
      ras.add_path(e);
      agg::render_scanlines(ras, sl, ren_sl);

      agg::trans_affine img_mtx;
      double rotation = v.x > 0 ? 0: agg::pi;
      rotation += (v.x? atan(v.y/v.x) :0) - agg::pi/4;
      img_mtx *= agg::trans_affine_translation(-75, -75);
      img_mtx *= agg::trans_affine_rotation(rotation);
      img_mtx *= agg::trans_affine_translation(+75, +75);
      img_mtx *= agg::trans_affine_scaling(2.0*(app.w*c.r)/150);
      img_mtx *= agg::trans_affine_translation(app.w*(c.x-c.r), app.w*(c.y-c.r));
      img_mtx.invert();

      agg::span_allocator<agg::rgba8> sa;

      typedef agg::span_interpolator_linear<> interpolator_type;
      typedef agg::image_accessor_clip<pixfmt_type> img_source_type;
      typedef agg::span_image_filter_rgba_2x2<img_source_type,
              interpolator_type> span_gen_type;
      interpolator_type interpolator(img_mtx);

      pixfmt_type img_pixf(app.rbuf_img(img_idx));
      img_source_type img_src(img_pixf, agg::rgba_pre(0, 0.4, 0, 0.5));
      agg::image_filter<agg::image_filter_kaiser> filter;
      span_gen_type sg(img_src, interpolator, filter);

      ras.add_path(e);
      agg::render_scanlines_aa(ras, sl, rbase, sa, sg);
   }

   void move(double t)
   {
      c.x += t*v.x;
      c.y += t*v.y;

      if (force_field > 0.3)
         force_field -= 0.02;

      /* Against walls */
      if (v.x < 0 && c.x-c.r <= 0)
         v.x = -v.x, force_field = 1.0;
      if (v.x > 0 && c.x+c.r >= 1)
         v.x = -v.x, force_field = 1.0;
      if (v.y < 0 && c.y-c.r <= 0)
         v.y = -v.y, force_field = 1.0;
      if (v.y > 0 && c.y+c.r >= AR)
         v.y = -v.y, force_field = 1.0;
   }

   void collide(Ball& other)
   {
      double other_dx = other.v.x;
      double other_dy = other.v.y;

      geometry::P this_v(c.x+v.x, c.y+v.y);
      geometry::P point;
      geometry::Line l(c, other.c);
      l.closest_point_on_line(this_v, point);

      v.x = this_v.x - point.x;
      v.y = this_v.y - point.y;
      other.v.x = point.x - c.x;
      other.v.y = point.y - c.y;

      geometry::P other_v(other.c.x + other_dx, other.c.y+other_dy);
      l.closest_point_on_line(other_v, point);
      other.v.x += other_v.x - point.x;
      other.v.y += other_v.y - point.y;
      v.x += point.x - other.c.x;
      v.y += point.y - other.c.y;

      force_field = 1.0;
      other.force_field = 1.0;
   }

   bool touchingTarget(Target& t)
   {
      return (t.x - c.x)*(t.x - c.x)+(t.y - c.y)*(t.y - c.y) < c.r*c.r;
   }
};


class AiControl: public Control 
{
public:
   void getAcc(double& acc_x, double& acc_y, Ball& b, Target& t)
   {
      bool goingSlow  = geometry::mod(b.v) < 0.001*level;
      bool goingRight = geometry::time_to_colision(
            b.c, geometry::Circle(t.x, t.y, 0),
            b.v, geometry::Vect(0,0)) < 5000;

      if (goingRight || goingSlow)
      {
         acc_x = 0.0002*(t.x - b.c.x)*level; 
         acc_y = 0.0002*(t.y - b.c.y)*level; 
         return;
      }
      acc_x = -0.2*b.v.x; 
      acc_y = -0.2*b.v.y; 
   }
};



class Control1: public Control
{
public:
   App& app;
   Control1(App& application):
      app(application) {}
   void getAcc(double& acc_x, double& acc_y, Ball& b, Target& t)
   {
      acc_x = 0; acc_y = 0;
      if (app.mouse_down)
      {
         acc_x = 0.002*(((double)app.mx)/app.w - b.c.x);
         acc_y = 0.002*(((double)app.my)/app.w - b.c.y);
      }
   }
};

class Control2: public Control
{
public:
   App& app;
   Control2(App& application, double size_, double x_, double y_):
      x(x_), y(y_), size(size_), app(application) {}
   void getAcc(double& acc_x, double& acc_y, Ball& b, Target& t)
   {
      acc_x = 0; acc_y = 0;
      double mx, my;
      bool active = false;

      if (app.mouse_down0 && d(app.mx0, app.my0) < 1.2*size)
      {
         mx = app.mx0;
         my = app.my0;
         active = true;
      }

      if (app.mouse_down1 && d(app.mx1, app.my1) < 1.2*size)
      {
         mx = app.mx1;
         my = app.my1;
         active = true;
      }
      
      if (active)
      {
         double dx = mx - x;
         double dy = my - y;
         double lt = 1;
         if (dx*dx+dy*dy > size*size)
         {
            lt = sqrt(size*size/(dx*dx+dy*dy));
         }
         acc_x = 0.004*(dx*lt);
         acc_y = 0.004*(dy*lt);
      }
   }
   void draw(agg::rasterizer_scanline_aa<>& ras,
         agg::scanline_u8& sl,
         renderer_base_type& rbase)
   {
      renderer_scanline_type ren_sl(rbase);
      ren_sl.color(lblue);
      agg::ellipse e1;
      agg::ellipse e2;
      ras.reset();
      e1.init(app.w*x, app.w*y, 4, 4, 30);
      e2.init(app.w*x, app.w*y, 60, 60, 30, true);
      ras.add_path(e1);
      ras.add_path(e2);
      agg::render_scanlines(ras, sl, ren_sl);
      double mx, my;
      bool active = false;

      if (app.mouse_down0 && d(app.mx0, app.my0) < 1.2*size)
      {
         mx = app.mx0;
         my = app.my0;
         active = true;
      }

      if (app.mouse_down1 && d(app.mx1, app.my1) < 1.2*size)
      {
         mx = app.mx1;
         my = app.my1;
         active = true;
      }
      
      if (active)
      {
         double dx = mx - x;
         double dy = my - y;
         double lt = 1;
         if (dx*dx+dy*dy > size*size)
         {
            lt = sqrt(size*size/(dx*dx+dy*dy));
         }
         double lx = x + dx*lt;
         double ly = y + dy*lt;
         ren_sl.color(red);
         agg::ellipse e3;
         e3.init(app.w*lx, app.w*ly, 10, 10, 20);
         ras.reset();
         ras.add_path(e3);
         agg::render_scanlines(ras, sl, ren_sl);
      }
   };
   double x, y, size;

private:
   double d(double mx, double my)
   {
      return sqrt((mx-x)*(mx-x)+(my-y)*(my-y));
   }
};

class Background
{
public:
   Background(App& application, agg::rgba c)
      : app(application), color(c), i(0)
   {
      on_resize();
   }
   void draw(
         agg::rasterizer_scanline_aa<>& ras,
         agg::scanline_u8& sl,
         renderer_base_type& rbase, bool blink) 
   {
      if (blink) {
         i = 10;
      }
      double f = i? (double)(i--)/10: 0;
      double f_1 = 1-f;
      agg::rgba c(
            f_1*color.r +f*lgray.r,
            f_1*color.g +f*lgray.g,
            f_1*color.b +f*lgray.b,
            f_1*color.a +f*lgray.a);
      rbase.clear(c);

      for (int i = 0; i < 50; i++)
      {
         renderer_scanline_type ren_sl(rbase);
         ren_sl.color(lgray);
         agg::ellipse e;
         ras.reset();
         double p = phase[i];
         e.init(s[i].x*app.w, s[i].y*app.w, 2+sin(p), 2+sin(p), 5);
         ras.add_path(e);
         agg::render_scanlines(ras, sl, ren_sl);
         phase[i] += 0.03;
      }
   }

   void on_resize() 
   {
      for (int i = 0; i < 50; i++)
      {
         s[i].x =      (double)rand()/RAND_MAX;
         s[i].y =   AR*(double)rand()/RAND_MAX;
         phase[i] =    (double)rand();
      }
   } 

   agg::rgba color;
   App& app;

   int i;
   geometry::P s[50];
   double phase[50];
};
