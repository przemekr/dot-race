/*
 * Dot-Race, the ...
 * Copyright 2014 Przemyslaw Rzepecki
 * Contact: przemekr@sdfeu.org
 * 
 * This file is part of Dot-Race.
 * 
 * Dot-Race is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 * 
 * Dot-Race is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * dot-race.  if not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <algorithm> 
#include <stack>
#include <unistd.h>
#include "agg_basics.h"
#include "agg_rendering_buffer.h"
#include "agg_rasterizer_scanline_aa.h"
#include "agg_scanline_u.h"
#include "agg_scanline_p.h"
#include "agg_renderer_scanline.h"
#include "agg_ellipse.h"
#include "agg_pixfmt_gray.h"
#include "agg_pixfmt_rgb.h"
#include "agg_pixfmt_amask_adaptor.h"
#include "agg_span_allocator.h"
#include "agg_span_gradient.h"
#include "agg_span_interpolator_linear.h"
#include "agg_span_image_filter_rgba.h"
#include "agg_pixfmt_rgba.h"
#include "agg_image_accessors.h"
#include "agg_conv_transform.h"
#include "agg_color_rgba.h"
#include "agg_gsv_text.h"
#include "agg_button_ctrl.h"
#include "platform/agg_platform_support.h"
#include <unistd.h>
#include <stdint.h>
#include "stdio.h"

#include <time.h>

#ifdef MOBILE
#define START_H  600
#define START_W  480
#define WINDOW_FLAGS agg::window_fullscreen | agg::window_keep_aspect_ratio
#else
#define START_H  600
#define START_W  480
#define WINDOW_FLAGS agg::window_resize | agg::window_hw_buffer
#endif

#define CTRL_TEXT_THICKNESS 3

#if __ANDROID__
#include <android/log.h>
#define DEBUG_PRINT(...) do{ __android_log_print(ANDROID_LOG_INFO, __FILE__, __VA_ARGS__ ); } while (false)
#else
#define DEBUG_PRINT(...) do{fprintf(stderr, __VA_ARGS__ ); } while (false)
#endif

enum flip_y_e { flip_y = true };
const agg::rgba transp(0, 0, 0, 0);
const agg::rgba dblue(0,0,200,128);
const agg::rgba lblue(0,0,128,100);
const agg::rgba lgray(60,60,60);
const agg::rgba green(0,120,0);
const agg::rgba yellow(63,63,0);
const agg::rgba red(0.9,0,0,0.7);
const agg::rgba black(0,0,0);
typedef agg::pixfmt_bgra32     pixfmt_type;
//typedef agg::pixfmt_abgr32   pixfmt_type;  // match bmp data.
typedef agg::renderer_base<pixfmt_type> renderer_base_type;
typedef agg::renderer_scanline_aa_solid<renderer_base_type> renderer_scanline_type;


class View
{
public:
   virtual void enter() {}
   virtual void on_draw() {}
   virtual void on_resize(int sx, int sy) {}
   virtual void on_idle() {}
   virtual void on_mouse_move(int x, int y, unsigned flags) {}
   virtual void on_multi_gesture(float dTheta, float dDist, float x,
         float y, int numFinders) {}
   virtual void on_touch_event(float x, float y, float dx, float dy, int id, bool down) {}
   virtual void on_mouse_button_down(int x, int y, unsigned flags) {}
   virtual void on_mouse_button_up(int x, int y, unsigned flags) {}
   virtual void on_key(int x, int y, unsigned key, unsigned flags) {}
   virtual void on_ctrl_change() {}
   agg::ctrl_container m_ctrls;
   void add_ctrl(agg::ctrl& c) { m_ctrls.add(c);}
};

class GameSettings
{
public:
   enum CtrlType {
      TOUCH_TO_GO,
      VIRT_JOYSTICK,
      AI_CONTROLLER
   };
   enum Player {
      PLAYER1,
      PLAYER2,
   };
   virtual void setController(Player, CtrlType, int level) = 0;
   bool newGame;
};


class App: public agg::platform_support
{
public:
   App(agg::pix_format_e format, bool flip_y) :
      agg::platform_support(format, flip_y),
      w(START_W), h(START_H), mouse_down(false),
      ctrlType(1), sound(false), music(false),
      p1Socore(0), p2Socore(0), 
      mouse_down0(false), mouse_down1(false)
   {}

   virtual void changeView(const char* name) = 0;
   virtual GameSettings& getGameSettings() = 0;

   int w, h;
   int mx, my; 
   double mx0, my0, mx1, my1;
   int ctrlType;
   bool mouse_down;
   bool mouse_down0, mouse_down1;
   bool sound, music;
   int p1Socore, p2Socore;

   void draw_text(double x, double y, double size, const char* str)
   {
      agg::rasterizer_scanline_aa<> m_ras;
      agg::scanline_p8              m_sl;
      pixfmt_type pf(rbuf_window());
      agg::renderer_base<pixfmt_type> rb(pf);
      agg::renderer_scanline_aa_solid<agg::renderer_base<pixfmt_type> > ren(rb);

      agg::gsv_text txt;
      agg::conv_stroke<agg::gsv_text> txt_stroke(txt);
      txt_stroke.width(size/6);
      txt_stroke.line_cap(agg::round_cap);
      txt.size(size);
      txt.start_point(x, y);
      txt.text(str);
      m_ras.add_path(txt_stroke);
      ren.color(agg::rgba(0.8, 0, 0, 0.6));
      agg::render_scanlines(m_ras, m_sl, ren);
   }

   virtual void on_ctrl_change()
   {
      view->on_ctrl_change();
   }

   virtual void on_resize(int nx, int ny)
   {
      printf("on_resize %d,%d\n", nx, ny);
      w = nx; h = ny;
      view->on_resize(nx, ny);
   }

   virtual void on_idle()
   {
      view->on_idle();
   }

   virtual void on_mouse_button_up(int x, int y, unsigned flags)
   {
      view->on_mouse_button_up(x, y, flags);
   }

   virtual void on_mouse_button_down(int x, int y, unsigned flags)
   {
      view->on_mouse_button_down(x, y, flags);
   }

   virtual void on_mouse_move(int x, int y, unsigned flags)
   {
      view->on_mouse_move(x, y, flags);
   }

   virtual void on_multi_gesture(float x, float y,
         float dTheta, float dDist, int numFingers)
   {
      view->on_multi_gesture(x, y, dTheta, dDist, numFingers);
   }
   virtual void on_touch_event(float x, float y, float dx, float dy,
         int id, bool down)
   {
      view->on_touch_event(x, y, dx, dy, id, down);
   }

   virtual void on_draw()
   {
      view->on_draw();
   }

protected:
   View* view;
};
