#include "ctrl/agg_slider_ctrl.h"
#include "ctrl/agg_rbox_ctrl.h"

class AboutView : public View
{
public:
   AboutView(App& application): app(application),
   background(app, black),
   exitMenu(20, 45,   130, 65,    "Return  ",  !flip_y)
   {
      exitMenu.background_color(red);
      add_ctrl(exitMenu);
   }

   virtual void on_resize(int, int)
   {
      background.on_resize();
   }

   virtual void on_draw()
   {
      typedef agg::rgba8 color_type;
      pixfmt_type  pf(app.rbuf_window());
      renderer_base_type rbase(pf);
      agg::rasterizer_scanline_aa<> ras;
      agg::scanline_u8 sl;
      ras.reset();
      background.draw(ras, sl, rbase, false);

      agg::render_ctrl(ras, sl, rbase, exitMenu);

      char string[200];
      sprintf(string, "Dot Racer v0.1");
      app.draw_text(40, 550, 35, string);

      sprintf(string, "You control a space ship!\n\n"
            "Hunt for stars, collect them\n\n"
            "before your opponent do.\n\n\n"
            "Tap your finger to accelerate the\n\n"
            "spaceship in that direction. To\n\n"
            "break tap behind your ship");
      app.draw_text(40, 500, 15, string);
   }

   virtual void on_ctrl_change()
   {
      if (exitMenu.status())
      {
         exitMenu.status(false);
         app.changeView("menu");
      }
   }

   virtual void on_idle()
   {
      app.wait_mode(true);
   }

   virtual void on_mouse_button_up(int x, int y, unsigned flags)
   {
      if (m_ctrls.on_mouse_button_up(x, y))
      {
         app.on_ctrl_change();
         app.force_redraw();
      }
   }

   virtual void on_mouse_button_down(int x, int y, unsigned flags)
   {
      if (m_ctrls.on_mouse_button_down(x, y))
      {
         app.on_ctrl_change();
         app.force_redraw();
         return;
      }
   }

   virtual void on_mouse_move(int x, int y, unsigned flags)
   {
      if (m_ctrls.on_mouse_move(x, y, (flags & agg::mouse_left) != 0))
      {
         app.on_ctrl_change();
         app.force_redraw();
         return;
      }
   }

private:
    App& app;
    Background background;
    agg::button_ctrl<agg::rgba8> exitMenu;
};
