#include "ctrl/agg_slider_ctrl.h"
#include "ctrl/agg_rbox_ctrl.h"
#include "ctrl/agg_cbox_ctrl.h"

class MenuView : public View
{
public:
   MenuView(App& application): app(application),
   background(app, black),
   exitApp (20, 20,   130, 40,    "Quit App",  !flip_y),
   exitMenu(20, 45,   130, 65,    "Return  ",  !flip_y),
   newGame (20, 70,   130, 90,    "New Game",  !flip_y),
   p1_ctr  (160, 20,  360, 35,    !flip_y),
   p2_ctr  (160, 50,  360, 65,    !flip_y),
   p1_ctrl_type(20, 100, 200, 160,   !flip_y),
   p2_ctrl_type(20, 200, 300, 160,   !flip_y),
   sound(200, 100,                "Sound  ",  !flip_y),
   music(200, 130,                "Music  ",  !flip_y),
   about(20, 500,  130, 520,      "About  ",  !flip_y),
   p1_wins(0), p2_wins(0)
   {
      p1_ctr.range(1, 8);
      p1_ctr.num_steps(8);
      p1_ctr.value(1);
      p1_ctr.label("P1 AI level: 1");
      p1_ctr.background_color(transp);
      p1_ctr.text_color(agg::rgba(0.9,0.0,0,9));
      p1_ctr.text_thickness(CTRL_TEXT_THICKNESS);
      p1_ctr.border_width(0, 0);
      p2_ctr.range(1, 8);
      p2_ctr.num_steps(8);
      p2_ctr.value(1);
      p2_ctr.label("P2 AI level: 1");
      p2_ctr.background_color(transp);
      p2_ctr.text_color(agg::rgba(0.9,0.9,0,9));
      p2_ctr.text_thickness(CTRL_TEXT_THICKNESS);
      p2_ctr.border_width(0, 0);
      exitMenu.background_color(red);
      exitApp.background_color(red);
      about.background_color(red);
      newGame.background_color(red);
      p1_ctrl_type.text_size(15);
      p1_ctrl_type.text_thickness(2);
      p1_ctrl_type.add_item("TapToGo");
      p1_ctrl_type.add_item("VirtJoystic");
      p1_ctrl_type.add_item("Computer");
      p1_ctrl_type.cur_item(0);
      p1_ctrl_type.background_color(transp);
      p1_ctrl_type.border_color(transp);
      p1_ctrl_type.text_color(red);

      p2_ctrl_type.text_size(15);
      p2_ctrl_type.text_thickness(2);
      p2_ctrl_type.add_item("TapToGo");
      p2_ctrl_type.add_item("VirtJoystic");
      p2_ctrl_type.add_item("Computer");
      p2_ctrl_type.cur_item(2);
      p2_ctrl_type.background_color(transp);
      p2_ctrl_type.border_color(transp);
      p2_ctrl_type.text_color(red);

      sound.text_size(15);
      sound.text_color(red);
      sound.text_thickness(CTRL_TEXT_THICKNESS);
      sound.active_color(red);
      sound.inactive_color(red);

      music.text_size(15);
      music.text_color(red);
      music.text_thickness(CTRL_TEXT_THICKNESS);
      music.active_color(red);
      music.inactive_color(red);

      add_ctrl(p1_ctr);
      add_ctrl(p2_ctr);
      add_ctrl(sound);
      add_ctrl(music);
      add_ctrl(exitMenu);
      add_ctrl(exitApp);
      add_ctrl(about);
      add_ctrl(newGame);
      add_ctrl(p1_ctrl_type);
      add_ctrl(p2_ctrl_type);
   }

   void enter()
   {
      app.wait_mode(true);
      background.on_resize();
      if (app.p1Socore > app.p2Socore)
      {
         p1_wins++;
         p2_ctr.value(p2_ctr.value()+1);
      }
      if (app.p2Socore > app.p1Socore)
      {
         p2_wins++;
         p1_ctr.value(p1_ctr.value()+1);
      }
   }

   virtual void on_resize(int, int)
   {
      background.on_resize();
   }

   virtual void on_draw()
   {
      typedef agg::rgba8 color_type;
      pixfmt_type  pf(app.rbuf_window());
      renderer_base_type rbase(pf);
      agg::rasterizer_scanline_aa<> ras;
      agg::scanline_u8 sl;
      ras.reset();
      background.draw(ras, sl, rbase, false);

      agg::render_ctrl(ras, sl, rbase, exitMenu);
      agg::render_ctrl(ras, sl, rbase, about);
      agg::render_ctrl(ras, sl, rbase, sound);
      agg::render_ctrl(ras, sl, rbase, music);
      agg::render_ctrl(ras, sl, rbase, exitApp);
      agg::render_ctrl(ras, sl, rbase, newGame);
      agg::render_ctrl(ras, sl, rbase, p1_ctr);
      agg::render_ctrl(ras, sl, rbase, p2_ctr);
      agg::render_ctrl(ras, sl, rbase, p1_ctrl_type);
      agg::render_ctrl(ras, sl, rbase, p2_ctrl_type);

      char string[200];
      sprintf(string, "Dot Racer v0.1");
      app.draw_text(40, 550, 35, string);

      sprintf(string, "Player1:%d  Player2:%d", p1_wins, p2_wins);
      app.draw_text(40, 450, 15, string);
   }
   virtual void on_ctrl_change()
   {
      if (exitMenu.status())
      {
         exitMenu.status(false);
         app.changeView("game");
      }
      if (newGame.status())
      {
         newGame.status(false);
         app.p1Socore = 0;
         app.p2Socore = 0;
         app.getGameSettings().newGame = true;
         app.changeView("game");
      }
      if (about.status())
      {
         about.status(false);
         app.changeView("about");
      }
      if (exitApp.status())
      {
         throw 0;
      }

      app.sound = sound.status();
      if (app.music != music.status())
      {
         app.play_music(rand()%4, music.status()? 40:0);
         app.music = music.status();
      }

      p1_ctr.label("P1 AI level: %1.0f");
      p2_ctr.label("P2 AI level: %1.0f");

      app.getGameSettings().setController(GameSettings::PLAYER1,
            itemToCtrl(p1_ctrl_type.cur_item()), p1_ctr.value());
      app.getGameSettings().setController(GameSettings::PLAYER2,
            itemToCtrl(p2_ctrl_type.cur_item()), p2_ctr.value());
   }

   virtual void on_mouse_button_up(int x, int y, unsigned flags)
   {
      if (m_ctrls.on_mouse_button_up(x, y))
      {
         app.on_ctrl_change();
         app.force_redraw();
      }
   }

   virtual void on_mouse_button_down(int x, int y, unsigned flags)
   {
      if (m_ctrls.on_mouse_button_down(x, y))
      {
         app.on_ctrl_change();
         app.force_redraw();
         return;
      }
   }

   virtual void on_mouse_move(int x, int y, unsigned flags)
   {
      if (m_ctrls.on_mouse_move(x, y, (flags & agg::mouse_left) != 0))
      {
         app.on_ctrl_change();
         app.force_redraw();
         return;
      }
   }

private:
    App& app;
    Background background;
    agg::cbox_ctrl<agg::rgba8>   sound;
    agg::cbox_ctrl<agg::rgba8>   music;
    agg::button_ctrl<agg::rgba8> about;
    agg::button_ctrl<agg::rgba8> exitMenu;
    agg::button_ctrl<agg::rgba8> exitApp;
    agg::button_ctrl<agg::rgba8> newGame;
    agg::slider_ctrl<agg::rgba8> p1_ctr;
    agg::slider_ctrl<agg::rgba8> p2_ctr;
    agg::rbox_ctrl  <agg::rgba8> p1_ctrl_type;
    agg::rbox_ctrl  <agg::rgba8> p2_ctrl_type;
    int p1_wins, p2_wins;

    GameSettings::CtrlType itemToCtrl (int t)
    {
       switch (t)
       {
          case 0:
             return GameSettings::TOUCH_TO_GO;
          case 1:
             return GameSettings::VIRT_JOYSTICK;
          case 2:
             return GameSettings::AI_CONTROLLER;
       }
    }
};
