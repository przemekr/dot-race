class GameView : public View, public GameSettings
{
public:
   GameView(App& application): app(application),
      menu(80,  20, 180, 35,    "Menu", !flip_y),
      pause(220, 20, 320, 35,   "Pause", !flip_y),
      blink(false),
      background(app, dblue),
      aicontrol(),
      control1(app),
      controlP1(app, 0.2, 0.2, 0.3*AR),
      controlP2(app, 0.2, 0.8, 0.8*AR),
      p1ball(app, 0.06, 0.4, 0.9, 0.5, agg::rgba(0.9,0.9,0), rand()%5, &control1),
      p2ball(app, 0.06, 0.8, 0.1, 0.5, agg::rgba(0.9,0.0,0), rand()%5, &aicontrol),
      time_left(0),
      target(app, 0.03, 0.1, 0.6, agg::rgba(0, 0.7, 0.9))
   {
      menu.background_color(red);
      pause.background_color(red);
      add_ctrl(menu);
      add_ctrl(pause);
      frame_cnt = 0;
      app.start_timer();
   }

   void enter()
   {
      app.wait_mode(false);
      background.on_resize();
   }

   virtual void on_ctrl_change()
   {
      if (pause.status())
      {
         app.wait_mode(!app.wait_mode());
      }

      if (menu.status())
      {
         menu.status(false);
         app.changeView("menu");
      }
      app.force_redraw();
   }

   virtual void on_resize(int, int)
   {
      background.on_resize();
      controlP1.y = 0.3*AR;
      controlP2.y = 0.8*AR;
   }

   virtual void on_idle()
   {
      const int MAX_FPS = 35;
      int loop_time = app.elapsed_time();
#ifndef PROFILING
      if (loop_time < 1000/MAX_FPS)
      {
         usleep(1000*(1000/MAX_FPS-loop_time));
      }
      time_left -= (double)app.elapsed_time()/1000;
      app.start_timer();
#else
      if (frame_cnt++ == 1000)
      {
         printf("frames %.2f/s - 1000 in %.2fms\n",
               (double)1000*frame_cnt/app.elapsed_time(),
               app.elapsed_time());
         app.start_timer();
         frame_cnt = 0;
      }
#endif
      update();
      app.force_redraw();
   }

   virtual void on_mouse_button_up(int x, int y, unsigned flags)
   {
      if (menu.on_mouse_button_up(x, y)
            || pause.on_mouse_button_up(x, y))
      {
         on_ctrl_change();
         return;
      }
      app.mouse_down = false;
   }

   virtual void on_mouse_button_down(int x, int y, unsigned flags)
   {
      if (menu.on_mouse_button_down(x, y)
            || pause.on_mouse_button_down(x, y))
      {
         on_ctrl_change();
         return;
      }
      app.mouse_down = true;
      app.mx = x;
      app.my = y;
   }

   virtual void on_mouse_move(int x, int y, unsigned flags)
   {
      app.mx = x;
      app.my = y;
   }

   void on_touch_event(float x, float y, float dx, float dy,
         int id, bool down)
   {
      if (id == 0)
      {
         if (!down)
            app.mouse_down0 = false;
         else
         {
            app.mouse_down0 = true;
            app.mx0 = x;
            app.my0 = y*AR;
         }
      }
      if (id == 1)
      {
         if (!down)
            app.mouse_down1 = false;
         else
         {
            app.mouse_down1 = true;
            app.mx1 = x;
            app.my1 = y*AR;
         }
      }
   }

   virtual void on_draw()
   {
      pixfmt_type pf(app.rbuf_window());;
      renderer_base_type rbase(pf);
      agg::rasterizer_scanline_aa<> ras;
      agg::scanline_u8 sl;
      ras.reset();
      background.draw(ras, sl, rbase, blink);
      blink = false;

      p1ball.controller->draw(ras, sl, rbase);
      p2ball.controller->draw(ras, sl, rbase);
      p1ball.draw(ras, sl, rbase);
      p2ball.draw(ras, sl, rbase);
      target.draw(ras, sl, rbase);

      char scores[100];
      sprintf(scores, "%.1f, p1:%d, p2:%d", time_left, app.p1Socore, app.p2Socore);
      app.draw_text(10, 60, 15, scores);

      agg::render_ctrl(ras, sl, rbase, pause);
      agg::render_ctrl(ras, sl, rbase, menu);
   }

   void setController(Player p, CtrlType t, int level)
   {
      ballByPlayer(p).controller = ctrlByType(t, p);
      ballByPlayer(p).controller->level = level;
   }

private:
    App& app;
    agg::button_ctrl<agg::rgba8> pause;
    agg::button_ctrl<agg::rgba8> menu;
    Ball p1ball;
    Ball p2ball;
    Target target;
    Control1 control1;
    AiControl aicontrol;
    Control2 controlP1;
    Control2 controlP2;
    Background background;
    bool blink;
    int frame_cnt;
    double time_left;

    Ball& ballByPlayer(Player p)
    {
       switch(p)
       {
          case PLAYER1:
             return p1ball;
          case PLAYER2:
             return p2ball;
       }

    }
    Control* ctrlByType(CtrlType t, Player p)
    {
       switch(t)
       {
          case TOUCH_TO_GO:
             return &control1;
          case VIRT_JOYSTICK:
             if (p == PLAYER1)
                return &controlP1;
             else
                return &controlP2;
          case AI_CONTROLLER:
             return &aicontrol;
       }
    }

    void update()
    {
       double acc_x = 0;
       double acc_y = 0;

       double t1 = geometry::time_to_colision(p1ball.c, p1ball.v, 1, AR);
       double t2 = geometry::time_to_colision(p2ball.c, p2ball.v, 1, AR);
       double t3 = geometry::time_to_colision(p1ball.c, p2ball.c,
             p1ball.v, p2ball.v);
       double t = std::min(1.0, std::min(std::min(t1, t2), t3));

       p1ball.move(t);
       p2ball.move(t);
       if (t == t3)
       {
          p1ball.collide(p2ball);
          p1ball.move(0.1*t);
          p2ball.move(0.1*t);
          if (app.sound)
             app.play_sound(1, 1500*geometry::mod(
                      p1ball.v.x-p2ball.v.x,
                      p1ball.v.y-p2ball.v.y));
       }
       p1ball.updateV(target, 1.0);
       p2ball.updateV(target, 1.0);


       if (p1ball.touchingTarget(target))
       {
          app.p1Socore++;
          blink = true;
          target.newLocation();
          if (app.sound)
             app.play_sound(2, 40);
       }
       if (p2ball.touchingTarget(target))
       {
          app.p2Socore++;
          blink = true;
          target.newLocation();
          if (app.sound)
             app.play_sound(2, 40);
       }

       /* Change music track from time to time...*/
       if (blink && app.music && (app.p1Socore + app.p2Socore) % 10 == 0)
       {
         app.play_music(rand()%4, 40);
       }

       if (newGame)
       {
          newGame = false;
          p1ball.v = geometry::Vect(0,0);
          p1ball.c.x = 0.9;
          p1ball.c.y = 0.5;

          p1ball.v = geometry::Vect(0,0);
          p2ball.c.x = 0.1;
          p2ball.c.y = 0.5;

          time_left = 60;
       }

       if (time_left <= 0)
       {
          app.changeView("game_end");
       }
    }
};
