/*
 * Dot-Race, the ...
 *
 * Copyright 2014 Przemyslaw Rzepecki
 * Contact: przemekr@sdfeu.org
 * 
 * This file is part of Dot-Race.
 * 
 * Dot-Race is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 * 
 * Dot-Race is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * dot-race if not, see <http://www.gnu.org/licenses/>.
 */

#include "app_support.h"
#include "geometry.h"
#include "objects.h"
#include "game_view.h"
#include "menu_view.h"
#include "about_view.h"
#include "game_end_view.h"


class the_application: public App
{
public:
   the_application(agg::pix_format_e format, bool flip_y) :
      App(format, flip_y)
   {
      game = new GameView(*this);
      menu = new MenuView(*this);
      about = new AboutView(*this);
      game_end = new GameEndView(*this);
      view = menu;
   }
   void changeView(const char* name) 
   {
      if (strcmp(name, "menu") == 0)
         view = menu;
      if (strcmp(name, "game") == 0)
         view = game;
      if (strcmp(name, "about") == 0)
         view = about;
      if (strcmp(name, "game_end") == 0)
         view = game_end;
      view->enter();
   };
   GameSettings& getGameSettings()
   {
      return *game;
   }

private:
   GameView* game;
   MenuView* menu;
   AboutView* about;
   GameEndView* game_end;
};



int agg_main(int argc, char* argv[])
{
    srand (time(NULL));
    the_application app(agg::pix_format_bgra32, flip_y);
    app.caption("DotRaceAGG");

    if (false
          || !app.load_img(0, "space1.png")
          || !app.load_img(1, "space2.png")
          || !app.load_img(2, "space3.png")
          || !app.load_img(3, "space4.png")
          || !app.load_img(4, "space5.png")
          || !app.load_music(0, "music_track_1.ogg")
          || !app.load_music(1, "music_track_2.ogg")
          || !app.load_music(2, "music_track_3.ogg")
          || !app.load_music(3, "music_track_4.ogg")
          || !app.load_sound(1, "collision.ogg")
          || !app.load_sound(2, "powerup.ogg")
          )
    {
        char buf[256];
        sprintf(buf, "There must be files space1.%s\n", app.img_ext());
        app.message(buf);
        return 1;
    }

    if (app.init(START_W, START_H, WINDOW_FLAGS))
    {
       try {
          return app.run();
       } catch (...) {
          return 0;
       } 
    }
    return 1;
}
