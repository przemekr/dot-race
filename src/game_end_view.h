#include <math.h>
#include <algorithm>
#include <stdlib.h>
#include <time.h>


class GameEndView : public View
{
public:
   GameEndView(App& application):
      app(application),
      blink(true),
      background(app, black),
      text_x(app.w), text_y(300)
   {
   }

   void enter()
   {
      background.on_resize();
   }

   virtual void on_resize(int, int)
   {
      background.on_resize();
   }

   virtual void on_idle()
   {
      const int MAX_FPS = 35;
      int loop_time = app.elapsed_time();
      if (loop_time < 1000/MAX_FPS)
      {
         usleep(1000*(1000/MAX_FPS-loop_time));
      }
      app.start_timer();
      update();
      app.force_redraw();
   }


   virtual void on_draw()
   {
      pixfmt_type pf(app.rbuf_window());;
      renderer_base_type rbase(pf);
      agg::rasterizer_scanline_aa<> ras;
      agg::scanline_u8 sl;
      ras.reset();
      background.draw(ras, sl, rbase, blink);
      blink = false;

      char text[100];
      sprintf(text, "P l a y e r %s W I N S!!!",
            app.p1Socore>app.p2Socore? "1": "2");
      app.draw_text(text_x, text_y, 25, text);
   }

private:
    App& app;
    Background background;
    bool blink;
    double text_x, text_y;

    void update()
    {
       text_x -= 1.6;
       text_y = 10*sin(0.1*text_x)+300;

       if (text_x <= -200)
       {
          text_x = app.w;
          app.changeView("menu");
       }
    }
};
